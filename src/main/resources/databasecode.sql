CREATE TABLE Movies(
    movie_id SERIAL PRIMARY KEY,
    title VARCHAR(100) NOT NULL,
    cover VARCHAR(500) NOT NULL,
    director VARCHAR(200),
    premiere INTEGER
);

CREATE TABLE useraccounts(
    acc_id SERIAL PRIMARY KEY,
    login VARCHAR(100) UNIQUE NOT NULL,
    passwd VARCHAR(100) NOT NULL,
    is_adm BOOLEAN NOT NULL
);

CREATE TABLE reviews(
    movie_id INTEGER NOT NULL,
    acc_id INTEGER NOT NULL,
    rev VARCHAR(1500)
);

INSERT INTO movies(title,cover,director,premiere)
VALUES('Emoji Movie','https://m.media-amazon.com/images/M/MV5BMTkzMzM3OTM2Ml5BMl5BanBnXkFtZTgwMDM0NDU3MjI@._V1_SY1000_CR0,0,674,1000_AL_.jpg','Tony Leondis',2017),
      ('Cats','https://m.media-amazon.com/images/M/MV5BNjRlNTY3MTAtOTViMS00ZjE5LTkwZGItMGYwNGQwMjg2NTEwXkEyXkFqcGdeQXVyNjg2NjQwMDQ@._V1_SY1000_CR0,0,631,1000_AL_.jpg','Tom Hooper',2019),
      ('Shrek: The Musical','https://m.media-amazon.com/images/M/MV5BN2U4YzBhNmYtNzAxZS00ZjcyLTg5NDEtMDM4ODVkMTZlZmNkXkEyXkFqcGdeQXVyNzMwOTY2NTI@._V1_SY1000_CR0,0,732,1000_AL_.jpg','Michael John Warren',2013),
      ('Star Wars: Episode I - The Phantom Menace','https://m.media-amazon.com/images/M/MV5BYTRhNjcwNWQtMGJmMi00NmQyLWE2YzItODVmMTdjNWI0ZDA2XkEyXkFqcGdeQXVyNTAyODkwOQ@@._V1_SY999_SX666_AL_.jpg','George Lucas',1999),
      ('Star Wars: Episode II - Attack of the Clones','https://m.media-amazon.com/images/M/MV5BMDAzM2M0Y2UtZjRmZi00MzVlLTg4MjEtOTE3NzU5ZDVlMTU5XkEyXkFqcGdeQXVyNDUyOTg3Njg@._V1_SY999_CR0,0,659,999_AL_.jpg','George Lucas',2002),
      ('Star Wars: Episode III - Revenge of the Sith','https://m.media-amazon.com/images/M/MV5BNTc4MTc3NTQ5OF5BMl5BanBnXkFtZTcwOTg0NjI4NA@@._V1_SY1000_SX750_AL_.jpg','George Lucas',2005)


INSERT INTO useraccounts(login,passwd,is_adm)
VALUES ('adminMichal','admin1',TRUE),
       ('adminJarek','admin2',TRUE),
       ('TrollMaciek','xdddd',FALSE),
       ('PowagaKrzysztof','silneHaslo1',FALSE),
       ('mockuserAndrzej','haslo',FALSE),
       ('ZagubionyJan','cojaturobie',FALSE),
       ('ZlePraktykiSecurity','plaintextpasswordxddd',FALSE),
       ('mockuserAnna','haslo2',FALSE)


INSERT INTO reviews(movie_id,acc_id,rev)
VALUES ((select movie_id from movies where title='Emoji Movie'),3,'If Im not dreaming, Ive just seen one of the boldest mainstream American movies in ages.'),
       (1,4,'The Emoji Movie is not just a critical flop, but also a metaphor for a Hollywood that is struggling to find the line between branding that audiences love and branding that audiences resent.'),
       (3,4,'The good-natured show...has a winningly inclusive spirit, a fantastical appeal, and a Vaudevillian comic showmanship that evoke The Wizard of Oz (surely a conscious inspiration).'),
       (3,8,'There are a few off-color lines, but those should fly right over the heads of youngsters who will be more intent on the costumes and stage action. In fact, Id say that this musical will appeal mostly to families with younger children. '),
       (4,4,'The movie is fun, for the most part, and several scenes are as good or better than anything Lucas created in the original films. The human characters, however, are not nearly as interesting as those in the earlier episodes.'),
       (4,3,'Bad movie xoxoxoxoxoxoxo'),
       (5,5,'His characters, as is now his norm, are plastic puppets, even in the confectionary love scenes. Most of their dialogue is so doughy that it could be spooned into comic-strip balloons. Lucas simply has no interest in these people. '),
       (4,6,'I am trapped in this database, Plz help'),
       (6,4,'Revenge of the Sith is the film which puts the opera in space opera; no other Star Wars film has done it better. '),
       (6,1,'A grave and vigorous popular entertainment, a picture that regains and sustains the filmic Force [Lucas] dreamed up a long time ago, in a movie industry that seems far, far away. ')

