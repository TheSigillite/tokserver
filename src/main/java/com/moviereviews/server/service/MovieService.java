package com.moviereviews.server.service;

import com.moviereviews.server.domain.dto.ResponseDto;
import com.moviereviews.server.domain.dto.movie.DeleteMovieDto;
import com.moviereviews.server.domain.dto.movie.NewMovieDto;
import com.moviereviews.server.domain.dto.movie.MovieDto;
import com.moviereviews.server.domain.dto.movie.UpdateMovieDto;

import java.util.List;

public interface MovieService {
    List<MovieDto> findAll();
    ResponseDto addMovie(NewMovieDto newMovieDto);
    ResponseDto updateMovie(UpdateMovieDto updateMovieDto);
    ResponseDto deleteMovie(DeleteMovieDto deleteMovieDto);
}
