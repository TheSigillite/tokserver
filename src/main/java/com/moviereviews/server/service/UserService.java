package com.moviereviews.server.service;

import com.moviereviews.server.domain.dto.ResponseDto;
import com.moviereviews.server.domain.dto.user.LoginUserDto;
import com.moviereviews.server.domain.dto.user.LoginUserResponseDto;
import com.moviereviews.server.domain.dto.user.ModUserDto;

public interface UserService {
    LoginUserResponseDto loginUser(LoginUserDto loginUser);
    ResponseDto registerUser(LoginUserDto registerUser);
    ResponseDto modUser(ModUserDto modUser);
}
