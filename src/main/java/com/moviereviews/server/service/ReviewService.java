package com.moviereviews.server.service;

import com.moviereviews.server.domain.dto.review.DeleteReviewDto;
import com.moviereviews.server.domain.dto.review.GetReviewDto;
import com.moviereviews.server.domain.dto.review.NewReviewDto;
import com.moviereviews.server.domain.dto.review.ReviewDto;
import com.moviereviews.server.domain.dto.ResponseDto;

import java.util.List;

public interface ReviewService {
    List<ReviewDto> findReviewsForMovie(Long movieid);
    ResponseDto deleteReview(DeleteReviewDto deleteReviewDto);
    ResponseDto addReview(NewReviewDto newReviewDto);
}
