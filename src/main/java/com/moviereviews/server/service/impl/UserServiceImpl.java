package com.moviereviews.server.service.impl;

import com.moviereviews.server.domain.dto.ResponseDto;
import com.moviereviews.server.domain.dto.user.LoginUserDto;
import com.moviereviews.server.domain.dto.user.LoginUserResponseDto;
import com.moviereviews.server.domain.dto.user.ModUserDto;
import com.moviereviews.server.domain.entity.User;
import com.moviereviews.server.domain.mapper.OneToOneConverter;
import com.moviereviews.server.domain.repository.UserRepository;
import com.moviereviews.server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    public final UserRepository userRepository;
    public final OneToOneConverter<LoginUserResponseDto, User> loginUserResponseMapper;
    public final OneToOneConverter<User, User> modUserMapper;

    @Autowired
    public UserServiceImpl(UserRepository userRepository
            ,OneToOneConverter<LoginUserResponseDto, User> loginUserResponseMapper
            ,OneToOneConverter<User, User> modUserMapper){
        this.userRepository = userRepository;
        this.loginUserResponseMapper = loginUserResponseMapper;
        this.modUserMapper = modUserMapper;
    }

    @Override
    public LoginUserResponseDto loginUser(LoginUserDto loginUser) {
        User logingin = userRepository.findUserByLoginAndPasswd(loginUser.getLogin(),loginUser.getPasswd());
        if(logingin == null){
            return null;
        }
        return loginUserResponseMapper.convert(logingin);
    }

    @Override
    public ResponseDto registerUser(LoginUserDto registerUser) {
        Optional<User> user = Optional.ofNullable(userRepository.findUserByLogin(registerUser.getLogin()));
        if(user.isPresent()){
            return new ResponseDto(false,"This username is already taken");
        }
        userRepository.insertUser(registerUser.getLogin(),registerUser.getPasswd());
        return new ResponseDto(true,"You were registered");
    }

    @Override
    public ResponseDto modUser(ModUserDto modUser) {
        Optional<User> granter = Optional.ofNullable(userRepository.findUserByLoginAndPasswd(modUser.getLogin(), modUser.getPasswd()));
        if(granter.isEmpty()){
            return new ResponseDto(false,"User granting permission does not exist");
        }
        if(granter.get().getIs_adm()){
            Optional<User> newmod = Optional.ofNullable(userRepository.findUserByLogin(modUser.getUsertomod()));
            if(newmod.isEmpty()){
                return new ResponseDto(false,"User to grant Moderator status does not exist");
            }
            if(newmod.get().getIs_adm()){
                return new ResponseDto(false,"User is already a Moderator");
            }
            userRepository.save(modUserMapper.convert(newmod.get()));
            return new ResponseDto(true,"User has been successfully made Moderator");
        }
        return new ResponseDto(false,"You do not have Moderator permission");
    }
}
