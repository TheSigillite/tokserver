package com.moviereviews.server.service.impl;

import com.moviereviews.server.domain.dto.ResponseDto;
import com.moviereviews.server.domain.dto.review.DeleteReviewDto;
import com.moviereviews.server.domain.dto.review.NewReviewDto;
import com.moviereviews.server.domain.dto.review.ReviewDto;
import com.moviereviews.server.domain.entity.Review;
import com.moviereviews.server.domain.entity.User;
import com.moviereviews.server.domain.mapper.TwoToOneConverter;
import com.moviereviews.server.domain.repository.ReviewRepository;
import com.moviereviews.server.domain.repository.UserRepository;
import com.moviereviews.server.service.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ReviewServiceImpl implements ReviewService {

    public final TwoToOneConverter<List<ReviewDto>,List<Review>,List<User>> reviewListMapper;
    public final TwoToOneConverter<ReviewDto, Review, User> reviewMapper;
    public final TwoToOneConverter<Review, User, NewReviewDto> newReviewMapper;
    public final ReviewRepository reviewRepository;
    public final UserRepository userRepository;

    @Autowired
    public ReviewServiceImpl(TwoToOneConverter<List<ReviewDto>, List<Review>, List<User>> reviewListMapper
            , TwoToOneConverter<ReviewDto, Review, User> reviewMapper
            , TwoToOneConverter<Review, User, NewReviewDto> newReviewMapper
            , ReviewRepository reviewRepository
            , UserRepository userRepository){
        this.reviewListMapper = reviewListMapper;
        this.reviewMapper = reviewMapper;
        this.newReviewMapper = newReviewMapper;
        this.reviewRepository = reviewRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List<ReviewDto> findReviewsForMovie(Long movieid) {
        List<Review> reviews = reviewRepository.findReviewsByMovie_id(movieid);
        return reviews.stream()
                .map(review -> reviewMapper.convert(review,userRepository.findUserByAcc_id(review.getAcc_id())))
                .collect(Collectors.toList());
    }

    @Override
    public ResponseDto deleteReview(DeleteReviewDto deleteReviewDto) {
        Optional<User> user = Optional.ofNullable(userRepository.findUserByLoginAndPasswd(deleteReviewDto.getLogin(), deleteReviewDto.getPasswd()));
        Review review = reviewRepository.findReviewByMovie_idAndRev(deleteReviewDto.getMovie_id(),deleteReviewDto.getRev());
        if(user.isEmpty()){
            return new ResponseDto(false,"User does not exist");
        }
        if(review.getAcc_id().equals(user.get().getAcc_id()) || user.get().getIs_adm()){
            reviewRepository.delete(review);
            return new ResponseDto(true,"Review Deleted");
        }
        return new ResponseDto(false,"You do not have Moderator permissions");
    }

    @Override
    public ResponseDto addReview(NewReviewDto newReviewDto) {
        Optional<User> user = Optional.ofNullable(userRepository.findUserByLoginAndPasswd(newReviewDto.getLogin(), newReviewDto.getPasswd()));
        if(user.isEmpty()){
            return new ResponseDto(false,"User does not exist");
        }
        Review review = newReviewMapper.convert(user.get(),newReviewDto);
        reviewRepository.insertReview(review.getMovie_id(),review.getAcc_id(),review.getRev());
        return new ResponseDto(true,"Review was posted, refresh page to see it");
    }
}
