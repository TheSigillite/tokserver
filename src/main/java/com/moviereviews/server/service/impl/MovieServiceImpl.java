package com.moviereviews.server.service.impl;

import com.moviereviews.server.domain.dto.ResponseDto;
import com.moviereviews.server.domain.dto.movie.DeleteMovieDto;
import com.moviereviews.server.domain.dto.movie.MovieDto;
import com.moviereviews.server.domain.dto.movie.NewMovieDto;
import com.moviereviews.server.domain.dto.movie.UpdateMovieDto;
import com.moviereviews.server.domain.entity.Movie;
import com.moviereviews.server.domain.entity.User;
import com.moviereviews.server.domain.mapper.OneToOneConverter;
import com.moviereviews.server.domain.repository.MovieRepository;
import com.moviereviews.server.domain.repository.ReviewRepository;
import com.moviereviews.server.domain.repository.UserRepository;
import com.moviereviews.server.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class MovieServiceImpl implements MovieService {
    private final MovieRepository movieRepository;
    private final UserRepository userRepository;
    private final ReviewRepository reviewRepository;
    private final OneToOneConverter<List<MovieDto>,List<Movie>> movieListMapper;
    private final OneToOneConverter<Movie, UpdateMovieDto> updateMovieDtoMapper;
    @Autowired
    public MovieServiceImpl(MovieRepository movieRepository
            , UserRepository userRepository
            , ReviewRepository reviewRepository
            , OneToOneConverter<List<MovieDto>,List<Movie>> movieListMapper
            , OneToOneConverter<Movie,UpdateMovieDto> updateMovieDtoMapper){
        this.movieRepository = movieRepository;
        this.userRepository = userRepository;
        this.reviewRepository = reviewRepository;
        this.movieListMapper = movieListMapper;
        this.updateMovieDtoMapper = updateMovieDtoMapper;
    }

    @Override
    public List<MovieDto> findAll() {
        List<Movie> movies = movieRepository.findAll(Sort.by(Sort.Direction.DESC, "premiere"));
        return movieListMapper.convert(movies);
    }

    @Override
    public ResponseDto addMovie(NewMovieDto newMovieDto) {
        try{
            Pattern regex = Pattern.compile("((http|https|ftp)://)?((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+");
            Matcher matcher = regex.matcher(newMovieDto.getCover());
            if(!matcher.find()) {
                throw new URISyntaxException(newMovieDto.getCover(), "Not a valid url");
            }
            URL u = new URL(newMovieDto.getCover());
            u.toURI();
        } catch (MalformedURLException | URISyntaxException e) {
            return new ResponseDto(false,"Invalid Image Link");
        }
        Optional<Movie> movie = Optional.ofNullable(movieRepository.findMovieByTitleAndDirectorAndPremiere(newMovieDto.getTitle(), newMovieDto.getDirector(), newMovieDto.getPremiere()));
        if (movie.isPresent()){
            return new ResponseDto(false,"That movie already exists");
        }
        User user = userRepository.findUserByLoginAndPasswd(newMovieDto.getLogin(),newMovieDto.getPasswd());
        //Check if user has admin privileges on site
        if(user == null){
            return new ResponseDto(false,"User does not exist");
        }
        if(user.getIs_adm()){
            movieRepository.insertMovie(newMovieDto.getTitle()
                    , newMovieDto.getCover()
                    , newMovieDto.getDirector()
                    , newMovieDto.getPremiere());
            return new ResponseDto(true,"Movie added successfully");
        }
        return new ResponseDto(false,"You do not have Moderator permissions");
    }

    @Override
    public ResponseDto updateMovie(UpdateMovieDto updateMovieDto) {
        try{
            Pattern regex = Pattern.compile("((http|https|ftp)://)?((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+");
            Matcher matcher = regex.matcher(updateMovieDto.getCover());
            if(!matcher.find()) {
                throw new URISyntaxException(updateMovieDto.getCover(), "Not a valid url");
            }
            URL u = new URL(updateMovieDto.getCover());
            u.toURI();
        } catch (MalformedURLException | URISyntaxException e) {
            return new ResponseDto(false,"Invalid Image Link");
        }
        Optional<User> user = Optional.ofNullable(userRepository.findUserByLoginAndPasswd(updateMovieDto.getLogin(), updateMovieDto.getPasswd()));
        Optional<Movie> mov = Optional.ofNullable(movieRepository.findMovieByMovie_id(updateMovieDto.getMovie_id()));
        if(mov.isEmpty()){
            return new ResponseDto(false, "This movie does not exist");
        }
        if(user.isEmpty()){
            return new ResponseDto(false,"User does not exist");
        }
        if (user.get().getIs_adm()){
            Movie movie = updateMovieDtoMapper.convert(updateMovieDto);
            movieRepository.save(movie);
            return new ResponseDto(true,"Movie has been successfully updated");
        }
        return new ResponseDto(false,"You do not have Moderator permissions");

    }

    @Override
    public ResponseDto deleteMovie(DeleteMovieDto deleteMovieDto) {
        Optional<User> user = Optional.ofNullable(userRepository.findUserByLoginAndPasswd(deleteMovieDto.getLogin(), deleteMovieDto.getPasswd()));
        if(user.isEmpty()){
            return new ResponseDto(false,"User does not exist");
        }
        if(user.get().getIs_adm()){
            movieRepository.deleteByMovie_id(deleteMovieDto.getMovie_id());
            reviewRepository.deleteByMovie_id(deleteMovieDto.getMovie_id());
            return new ResponseDto(true,"Movie and reviews of that movie have been deleted");
        }
        return new ResponseDto(false,"You do not have Moderator permissions");
    }

}
