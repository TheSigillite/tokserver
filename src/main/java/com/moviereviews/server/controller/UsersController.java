package com.moviereviews.server.controller;

import com.moviereviews.server.domain.dto.ResponseDto;
import com.moviereviews.server.domain.dto.user.LoginUserDto;
import com.moviereviews.server.domain.dto.user.LoginUserResponseDto;
import com.moviereviews.server.domain.dto.user.ModUserDto;
import com.moviereviews.server.service.UserService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(value = "/users")
public class UsersController {
    private final UserService userService;

    public UsersController(UserService userService){
        this.userService = userService;
    }

    @ApiOperation(value = "Loguje użytkownika"
            , notes = "Zwraca czy użytkownik jets moderatorem, jeśli nie istnieje zwraca błąd i pustą odpowiedź")
    @CrossOrigin
    @PostMapping(value = "/login",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> loginUser(
            @ApiParam(value = "Login i hasło użytkownika",required = true)
            @RequestBody LoginUserDto loginUserDto){
        LoginUserResponseDto loginUser = userService.loginUser(loginUserDto);
        if(loginUser == null){
            return new ResponseEntity<HttpEntity>(HttpEntity.EMPTY, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(loginUser, HttpStatus.OK);
    }

    @ApiOperation(value = "Dodaje nowego użytkownika", response = ResponseDto.class)
    @CrossOrigin
    @PostMapping(value = "/register",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ResponseDto> registerUser(
            @ApiParam(value = "Login i hasło nowego użytkownika", required = true)
            @RequestBody LoginUserDto loginUserDto){
        ResponseDto registerresponse = userService.registerUser(loginUserDto);
        if(registerresponse.getWasSuccesful()){
            return new ResponseEntity<>(registerresponse,HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(registerresponse,HttpStatus.BAD_REQUEST);
        }

    }

    @ApiOperation(value = "Nadaje użytkownikowi uprawinienia moderatora"
            , notes = "Operacja musi zostać przeprowadzona przez istniejącego moderatora", response = ResponseDto.class)
    @CrossOrigin
    @PutMapping(value = "/moderation",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ResponseDto> modUser(
            @ApiParam(value = "Login nowego moderatora i dane moderatora nadającego uprawnienia")
            @RequestBody ModUserDto modUserDto){
        ResponseDto modResponse = userService.modUser(modUserDto);
        if(modResponse.getWasSuccesful()){
            return new ResponseEntity<>(modResponse,HttpStatus.OK);
        } else {
            return new ResponseEntity<>(modResponse,HttpStatus.FORBIDDEN);
        }

    }
}
