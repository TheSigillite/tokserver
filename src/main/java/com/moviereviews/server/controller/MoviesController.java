package com.moviereviews.server.controller;


import com.moviereviews.server.domain.dto.ResponseDto;
import com.moviereviews.server.domain.dto.movie.DeleteMovieDto;
import com.moviereviews.server.domain.dto.movie.NewMovieDto;
import com.moviereviews.server.domain.dto.movie.MovieDto;
import com.moviereviews.server.domain.dto.movie.UpdateMovieDto;
import com.moviereviews.server.service.MovieService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/movies")
public class MoviesController {

    private final MovieService movieService;

    @Autowired
    public MoviesController(MovieService movieService){
        this.movieService = movieService;
    }

    @ApiOperation(value = "Zwraca liste wszystkich filmów na stronie")
    @CrossOrigin
    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<MovieDto>> getMovies(){
        return new ResponseEntity<>(movieService.findAll(),HttpStatus.OK);
    }
    @ApiOperation(value = "Dodaj nowy film",notes = "Metoda weryfikuje czy dodający ma uprawnienia",response = ResponseDto.class)
    @CrossOrigin
    @PostMapping(value = "/new", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ResponseDto> addMovie(
            @ApiParam(value = "Detale nowego filmu i dane dodającego", required = true)
            @RequestBody NewMovieDto newMovieDto){
        ResponseDto wascreated = movieService.addMovie(newMovieDto);
        if(wascreated.getWasSuccesful()){
            return new ResponseEntity<>(wascreated,HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(wascreated,HttpStatus.FORBIDDEN);
        }
    }
    @ApiOperation(value = "Edytuje istniejący film",notes = "Metoda weryfikuje czy edytujący ma uprawnienia", response = ResponseDto.class)
    @CrossOrigin
    @PutMapping(value = "/update", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ResponseDto> updateMovie(
            @ApiParam(value = "Detale edytowanego filmu i dane edytującego", required = true)
            @RequestBody UpdateMovieDto updateMovieDto){
        ResponseDto updated = movieService.updateMovie(updateMovieDto);
        if(updated.getWasSuccesful()){
            return new ResponseEntity<>(updated,HttpStatus.OK);
        } else {
            return new ResponseEntity<>(updated,HttpStatus.BAD_REQUEST);
        }
    }
    @ApiOperation(value = "Usuwa istniejący film", notes = "Usuwa również recenzje tego filmu", response = ResponseDto.class)
    @CrossOrigin
    @DeleteMapping(value = "/delete", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ResponseDto> deleteMovie(
            @ApiParam(value = "Id usuwanego pliku i dane usuwającego", required = true)
            @RequestBody DeleteMovieDto deleteMovieDto){
        ResponseDto deleted = movieService.deleteMovie(deleteMovieDto);
        if(deleted.getWasSuccesful()){
            return new ResponseEntity<>(deleted,HttpStatus.OK);
        } else {
            return new ResponseEntity<>(deleted,HttpStatus.NOT_FOUND);
        }
    }
}
