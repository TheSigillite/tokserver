package com.moviereviews.server.controller;

import com.moviereviews.server.domain.dto.ResponseDto;
import com.moviereviews.server.domain.dto.review.DeleteReviewDto;
import com.moviereviews.server.domain.dto.review.GetReviewDto;
import com.moviereviews.server.domain.dto.review.NewReviewDto;
import com.moviereviews.server.domain.dto.review.ReviewDto;
import com.moviereviews.server.service.ReviewService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/reviews")
public class ReviewsController {
    private final ReviewService reviewService;

    @Autowired
    public ReviewsController(ReviewService reviewService){
        this.reviewService = reviewService;
    }

    @ApiOperation(value = "Zwraca Liste recenzji danego filmu")
    @CrossOrigin
    @GetMapping(value = "/get", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<ReviewDto>> getMovieReviews(
            @ApiParam(value = "Id filmu dla którego mają być zwrócone recenzje", required = true)
            @RequestParam(name = "movieid") Long movieid){
        return new ResponseEntity<>(reviewService.findReviewsForMovie(movieid), HttpStatus.OK);
    }

    @ApiOperation(value = "Dodaje nową recenzje", notes = "Metoda weryfikuje czy konto dodające recenzje istnieje",response = ResponseDto.class)
    @CrossOrigin
    @PostMapping(value = "/new", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ResponseDto> newReview(
            @ApiParam(value = "Treść recenzji, id filmu i detale konta dodającego ocene", required = true)
            @RequestBody NewReviewDto newReviewDto){
        ResponseDto created = reviewService.addReview(newReviewDto);
        if(created.getWasSuccesful()){
            return new ResponseEntity<>(created,HttpStatus.CREATED);
        }else {
            return new ResponseEntity<>(created,HttpStatus.I_AM_A_TEAPOT);
        }
    }

    @ApiOperation(value = "Usuwa recenzje", notes = "Usuwa pojedynczą recenzje danego filmu",response = ResponseDto.class)
    @CrossOrigin
    @DeleteMapping(value = "/delete", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ResponseDto> deleteReview(
            @ApiParam(value = "Informacje o recenzji i kącie usuwającym recenzje")
            @RequestBody DeleteReviewDto deleteReviewDto){
        ResponseDto deleted = reviewService.deleteReview(deleteReviewDto);
        if(deleted.getWasSuccesful()){
            return new ResponseEntity<>(deleted,HttpStatus.OK);
        } else {
            return new ResponseEntity<>(deleted,HttpStatus.FORBIDDEN);
        }
    }
}
