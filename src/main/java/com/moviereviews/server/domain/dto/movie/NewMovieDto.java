package com.moviereviews.server.domain.dto.movie;

import com.moviereviews.server.domain.dto.user.LoginUserDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(description = "Typ używany do dodania nowego filmu")
public class NewMovieDto implements Serializable {
    @ApiModelProperty(notes = "Login dodającego")
    private String login;
    @ApiModelProperty(notes = "Hasło dodającego")
    private String passwd;
    @ApiModelProperty(notes = "Tytuł filmu")
    private String title;
    @ApiModelProperty(notes = "URL okładki filmu")
    private String cover;
    @ApiModelProperty(notes = "Reżyser filmu")
    private String director;
    @ApiModelProperty(notes = "Data premiery")
    private Integer premiere;

    public NewMovieDto(){
    }


    public String getLogin() {
        return login;
    }

    public String getPasswd() {
        return passwd;
    }

    public String getTitle() {
        return title;
    }


    public String getCover() {
        return cover;
    }


    public String getDirector() {
        return director;
    }


    public Integer getPremiere() {
        return premiere;
    }

}
