package com.moviereviews.server.domain.dto.review;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(description = "Typ używany do pobrania recenzji")
public class GetReviewDto implements Serializable {
    @ApiModelProperty(notes = "Id filmu")
    private Long movie_id;

    public GetReviewDto(){}

    public Long getMovie_id() {
        return movie_id;
    }

}
