package com.moviereviews.server.domain.dto.movie;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(description = "Typ w jakim zwracane są filmy")
public class MovieDto implements Serializable {
    @ApiModelProperty(notes = "Id filmu")
    private Long movie_id;
    @ApiModelProperty(notes = "Tytuł filmu")
    private String title;
    @ApiModelProperty(notes = "Adres URL zdjęcia okładkowego")
    private String cover;
    @ApiModelProperty(notes = "Reżyser filmu")
    private String director;
    @ApiModelProperty(notes = "Data premiery")
    private Integer premiere;

    public MovieDto(Builder builer){
        movie_id = builer.movie_id;
        title = builer.title;
        cover = builer.cover;
        director = builer.director;
        premiere = builer.premiere;
    }

    public Long getMovie_id() {
        return movie_id;
    }


    public String getTitle() {
        return title;
    }


    public String getCover() {
        return cover;
    }


    public String getDirector() {
        return director;
    }


    public Integer getPremiere() {
        return premiere;
    }


    public static final class Builder {
        private Long movie_id;
        private String title;
        private String cover;
        private String director;
        private Integer premiere;

        public Builder(){
        }

        public Builder movie_id(Long movie_id){
            this.movie_id = movie_id;
            return this;
        }
        public Builder title(String title){
            this.title = title;
            return this;
        }
        public Builder cover(String cover){
            this.cover = cover;
            return this;
        }
        public Builder director(String director){
            this.director = director;
            return this;
        }
        public Builder premiere(Integer premiere){
            this.premiere = premiere;
            return this;
        }
        public MovieDto build() {
            return new MovieDto(this);
        }
    }
}
