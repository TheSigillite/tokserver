package com.moviereviews.server.domain.dto.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
@ApiModel(description = "Dane do dodawania moderatora")
public class ModUserDto implements Serializable {
    @ApiModelProperty(notes = "Login dodającego")
    private String login;
    @ApiModelProperty(notes = "Hasło dodającego")
    private String passwd;
    @ApiModelProperty(notes = "Login użytkownika który ma zostać nowym moderatorem")
    private String usertomod;

    public ModUserDto(){
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public String getUsertomod() {
        return usertomod;
    }

    public void setUsertomod(String usertomod) {
        this.usertomod = usertomod;
    }
}
