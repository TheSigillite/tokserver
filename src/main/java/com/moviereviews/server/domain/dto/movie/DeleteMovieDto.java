package com.moviereviews.server.domain.dto.movie;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(description = "Typ używany przy usuwaniu filmu")
public class DeleteMovieDto implements Serializable {
    @ApiModelProperty(notes = "Login usuwającego")
    private String login;
    @ApiModelProperty(notes = "Hasło usuwającego")
    private String passwd;
    @ApiModelProperty(notes = "Id filmu do usunięcia")
    private Long movie_id;
    public DeleteMovieDto(){
    }

    public String getLogin() {
        return login;
    }

    public String getPasswd() {
        return passwd;
    }

    public Long getMovie_id() {
        return movie_id;
    }

}
