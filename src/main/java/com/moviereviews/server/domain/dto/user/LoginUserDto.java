package com.moviereviews.server.domain.dto.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(description = "Objekt używany przy logowaniu i rejestracj")
public class LoginUserDto implements Serializable {
    @ApiModelProperty(notes = "Login użytkownika")
    private String login;
    @ApiModelProperty(notes = "Hasło użytkownika")
    private String passwd;

    public LoginUserDto(){
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }
}
