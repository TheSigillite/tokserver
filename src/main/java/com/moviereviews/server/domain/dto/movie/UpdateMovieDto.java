package com.moviereviews.server.domain.dto.movie;

import com.moviereviews.server.domain.dto.user.LoginUserDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(description = "Typ używany do edycji filmu")
public class UpdateMovieDto implements Serializable {
    @ApiModelProperty(notes = "Login dodającego")
    private String login;
    @ApiModelProperty(notes = "Hasło dodającego")
    private String passwd;
    @ApiModelProperty(notes = "Id filmu")
    private Long movie_id;
    @ApiModelProperty(notes = "Tytuł filmu")
    private String title;
    @ApiModelProperty(notes = "URL okładki filmu")
    private String cover;
    @ApiModelProperty(notes = "Reżyser filmu")
    private String director;
    @ApiModelProperty(notes = "Data premiery")
    private Integer premiere;

    public UpdateMovieDto(){
    }


    public String getLogin() {
        return login;
    }

    public String getPasswd() {
        return passwd;
    }

    public Long getMovie_id() {
        return movie_id;
    }


    public String getTitle() {
        return title;
    }


    public String getCover() {
        return cover;
    }


    public String getDirector() {
        return director;
    }


    public Integer getPremiere() {
        return premiere;
    }

}
