package com.moviereviews.server.domain.dto.review;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(description = "Typ używany do dodawania recenzji")
public class NewReviewDto implements Serializable {
    @ApiModelProperty(notes = "Login dodającego")
    private String login;
    @ApiModelProperty(notes = "Hasło dodającego")
    private String passwd;
    @ApiModelProperty(notes = "Id filmu pod którym ma sie znaleźć recenzja")
    private Long movie_id;
    @ApiModelProperty(notes = "Treść recenzji")
    private String rev;

    public NewReviewDto() {
    }

    public String getLogin() {
        return login;
    }

    public String getPasswd() {
        return passwd;
    }

    public Long getMovie_id() {
        return movie_id;
    }

    public String getRev() {
        return rev;
    }

}
