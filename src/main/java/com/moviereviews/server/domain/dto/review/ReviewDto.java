package com.moviereviews.server.domain.dto.review;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(description = "Model recenzji która będzie wyświetlana pod filmem")
public class ReviewDto implements Serializable {
    @ApiModelProperty(notes = "Id filmu pod którym znajduje się recenzja")
    private Long movie_id;
    @ApiModelProperty(notes = "Login osoby która zostawiła recenzje")
    private String login;
    @ApiModelProperty(notes = "Treść recenzji")
    private String rev;

    public ReviewDto(Builder builder){
        movie_id = builder.movie_id;
        login = builder.login;
        rev = builder.rev;
    }

    public Long getMovie_id() {
        return movie_id;
    }

    public String getLogin() {
        return login;
    }

    public String getRev() {
        return rev;
    }

    public static final class Builder{
        private Long movie_id;
        private String login;
        private String rev;

        public Builder(){
        }

        public Builder movie_id(Long movie_id){
            this.movie_id = movie_id;
            return this;
        }

        public Builder login(String login){
            this.login = login;
            return this;
        }

        public Builder rev(String rev){
            this.rev = rev;
            return this;
        }

        public ReviewDto Build(){
            return new ReviewDto(this);
        }
    }
}
