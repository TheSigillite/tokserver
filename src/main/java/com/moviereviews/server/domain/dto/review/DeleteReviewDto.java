package com.moviereviews.server.domain.dto.review;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(description = "Typ używany do usunięcia recenzji")
public class DeleteReviewDto implements Serializable {
    @ApiModelProperty(notes = "Login usuwającego")
    private String login;
    @ApiModelProperty(notes = "Hasło usuwającego")
    private String passwd;
    @ApiModelProperty(notes = "Id filmu pod którym znajduje się recenzja")
    private Long movie_id;
    @ApiModelProperty(notes = "Treść recenzji")
    private String rev;

    public DeleteReviewDto() {
    }

    public String getLogin() {
        return login;
    }


    public String getPasswd() {
        return passwd;
    }


    public Long getMovie_id() {
        return movie_id;
    }


    public String getRev() {
        return rev;
    }

}
