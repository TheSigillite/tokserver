package com.moviereviews.server.domain.dto.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(description = "Odpowiedź na zalogowanie użytkownika")
public class LoginUserResponseDto implements Serializable {
    @ApiModelProperty(notes = "Czy użytkownik jest moderatorem")
    private Boolean isAdmin;

    private LoginUserResponseDto(Builder builder){
        this.isAdmin = builder.isAdmin;
    }

    public Boolean getAdmin() {
        return isAdmin;
    }

    public static final class Builder{
        private Boolean isAdmin;
        public Builder(){
        }

        public Builder isAdmin(Boolean isAdmin){
            this.isAdmin = isAdmin;
            return this;
        }

        public LoginUserResponseDto build(){
            return new LoginUserResponseDto(this);
        }
    }
}
