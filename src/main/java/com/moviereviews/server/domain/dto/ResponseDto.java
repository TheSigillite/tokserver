package com.moviereviews.server.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(description = "Detale o przebiegu operacji")
public class ResponseDto implements Serializable {
    @ApiModelProperty(notes = "Informacja czy operacja przebiegła pomyślnie")
    private Boolean wasSuccesful;
    @ApiModelProperty(notes = "Komunikat z którym zakończyło się wykonanie operacji")
    private String details;

    public ResponseDto(Boolean wasSuccesful, String details) {
        this.wasSuccesful = wasSuccesful;
        this.details = details;
    }

    public Boolean getWasSuccesful() {
        return wasSuccesful;
    }

    public String getDetails() {
        return details;
    }

}
