package com.moviereviews.server.domain.entity;

import java.io.Serializable;
import java.util.Objects;

public class ReviewId implements Serializable {
    private Long movie_id;
    private Long acc_id;
    private String rev;
    public ReviewId(){}

    public Long getMovie_id() {
        return movie_id;
    }

    public void setMovie_id(Long movie_id) {
        this.movie_id = movie_id;
    }

    public Long getAcc_id() {
        return acc_id;
    }

    public void setAcc_id(Long acc_id) {
        this.acc_id = acc_id;
    }

    public String getRev() {
        return rev;
    }

    public void setRev(String rev) {
        this.rev = rev;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReviewId reviewId = (ReviewId) o;
        return movie_id.equals(reviewId.movie_id) &&
                acc_id.equals(reviewId.acc_id) &&
                rev.equals(reviewId.rev);
    }

    @Override
    public int hashCode() {
        return Objects.hash(movie_id, acc_id, rev);
    }
}
