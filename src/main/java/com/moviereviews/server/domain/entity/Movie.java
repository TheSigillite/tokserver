package com.moviereviews.server.domain.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "movies")
public class Movie implements Serializable {
    @Id
    @Column(name = "movie_id")
    private Long movie_id;

    @Column(name = "title")
    private String title;

    @Column(name = "cover")
    private String cover;

    @Column(name = "director")
    private String director;

    @Column(name = "premiere")
    private Integer premiere;

    public Movie() {
    }

    public Movie(Builder builder) {
        movie_id = builder.movie_id;
        title = builder.title;
        cover = builder.cover;
        director = builder.director;
        premiere = builder.premiere;
    }

    public Long getMovie_id() {
        return movie_id;
    }


    public String getTitle() {
        return title;
    }


    public String getCover() {
        return cover;
    }


    public String getDirector() {
        return director;
    }


    public Integer getPremiere() {
        return premiere;
    }


    public static final class Builder{
        private Long movie_id;
        private String title;
        private String cover;
        private String director;
        private Integer premiere;
        public Builder(){
        }
        public Builder movie_id(Long movie_id){
            this.movie_id = movie_id;
            return this;
        }

        public Builder title(String title){
            this.title = title;
            return this;
        }

        public Builder cover(String cover){
            this.cover = cover;
            return this;
        }
        public Builder director(String director){
            this.director = director;
            return this;
        }

        public Builder premiere(Integer premiere){
            this.premiere = premiere;
            return this;
        }

        public Movie build(){
            return new Movie(this);
        }
    }
}
