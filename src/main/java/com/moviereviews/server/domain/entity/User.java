package com.moviereviews.server.domain.entity;

import javax.persistence.*;
import java.io.Serializable;
@Entity
@Table(name = "useraccounts")
public class User implements Serializable {
    @Id
    @Column(name = "acc_id")
    private Long acc_id;

    @Column(name = "login")
    private String login;

    @Column(name = "passwd")
    private String passwd;

    @Column(name = "is_adm")
    private Boolean is_adm;

    public User(){}

    private User(Builder builder){
        this.acc_id = builder.acc_id;
        this.login = builder.login;
        this.passwd = builder.passwd;
        this.is_adm = builder.is_adm;
    }

    public Long getAcc_id() {
        return acc_id;
    }


    public String getLogin() {
        return login;
    }


    public String getPasswd() {
        return passwd;
    }


    public Boolean getIs_adm() {
        return is_adm;
    }

    public void setAcc_id(Long acc_id) {
        this.acc_id = acc_id;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public void setIs_adm(Boolean is_adm) {
        this.is_adm = is_adm;
    }

    public static final class Builder{
        private Long acc_id;
        private String login;
        private String passwd;
        private Boolean is_adm;

        public Builder(){
        }

        public Builder acc_id(Long acc_id){
            this.acc_id = acc_id;
            return this;
        }

        public Builder login(String login){
            this.login = login;
            return this;
        }

        public Builder passwd(String passwd){
            this.passwd = passwd;
            return this;
        }

        public Builder is_adm(Boolean is_adm){
            this.is_adm = is_adm;
            return this;
        }

        public User build(){
            return new User(this);
        }
    }
}
