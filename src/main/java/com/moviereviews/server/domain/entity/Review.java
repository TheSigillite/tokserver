package com.moviereviews.server.domain.entity;

import javax.persistence.*;
import java.io.Serializable;
@Entity
@Table(name = "reviews")
@IdClass(ReviewId.class)
public class Review implements Serializable {
    @Id
    @Column(name = "movie_id")
    private Long movie_id;
    @Id
    @Column(name = "acc_id")
    private Long acc_id;
    @Id
    @Column(name = "rev")
    private String rev;

    public Review(){
    }

    public Review(Builder builder){
        movie_id = builder.movie_id;
        acc_id = builder.acc_id;
        rev = builder.rev;
    }

    public Long getMovie_id() {
        return movie_id;
    }

    public void setMovie_id(Long movie_id) {
        this.movie_id = movie_id;
    }

    public Long getAcc_id() {
        return acc_id;
    }

    public void setAcc_id(Long acc_id) {
        this.acc_id = acc_id;
    }

    public String getRev() {
        return rev;
    }

    public void setRev(String rev) {
        this.rev = rev;
    }

    public static final class Builder{
        private Long movie_id;
        private Long acc_id;
        private String rev;

        public Builder(){
        }

        public Builder movie_id(Long movie_id){
            this.movie_id = movie_id;
            return this;
        }

        public Builder acc_id(Long acc_id){
            this.acc_id = acc_id;
            return this;
        }

        public Builder rev(String rev){
            this.rev = rev;
            return this;
        }

        public Review build(){
            return new Review(this);
        }
    }
}
