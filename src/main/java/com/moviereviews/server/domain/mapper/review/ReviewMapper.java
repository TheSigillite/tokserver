package com.moviereviews.server.domain.mapper.review;

import com.moviereviews.server.domain.dto.review.ReviewDto;
import com.moviereviews.server.domain.entity.Review;
import com.moviereviews.server.domain.entity.User;
import com.moviereviews.server.domain.mapper.TwoToOneConverter;
import org.springframework.stereotype.Component;

@Component
public class ReviewMapper implements TwoToOneConverter<ReviewDto, Review, User> {

    @Override
    public ReviewDto convert(Review review, User user) {
        return new ReviewDto.Builder()
                .movie_id(review.getMovie_id())
                .login(user.getLogin())
                .rev(review.getRev())
                .Build();
    }
}
