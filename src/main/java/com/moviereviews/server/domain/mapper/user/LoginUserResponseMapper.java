package com.moviereviews.server.domain.mapper.user;

import com.moviereviews.server.domain.dto.user.LoginUserResponseDto;
import com.moviereviews.server.domain.entity.User;
import com.moviereviews.server.domain.mapper.OneToOneConverter;
import org.springframework.stereotype.Component;

@Component
public class LoginUserResponseMapper implements OneToOneConverter<LoginUserResponseDto, User> {
    @Override
    public LoginUserResponseDto convert(User user) {
        return new LoginUserResponseDto.Builder().isAdmin(user.getIs_adm()).build();
    }
}
