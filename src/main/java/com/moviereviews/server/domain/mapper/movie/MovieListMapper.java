package com.moviereviews.server.domain.mapper.movie;

import com.moviereviews.server.domain.dto.movie.MovieDto;
import com.moviereviews.server.domain.entity.Movie;
import com.moviereviews.server.domain.mapper.OneToOneConverter;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class MovieListMapper implements OneToOneConverter<List<MovieDto>,List<Movie>> {
    @Override
    public List<MovieDto> convert(List<Movie> movies) {
        return movies.stream().map(movie -> new MovieDto.Builder()
                .movie_id(movie.getMovie_id())
                .title(movie.getTitle())
                .cover(movie.getCover())
                .director(movie.getDirector())
                .premiere(movie.getPremiere())
                .build()).collect(Collectors.toList());
    }
}
