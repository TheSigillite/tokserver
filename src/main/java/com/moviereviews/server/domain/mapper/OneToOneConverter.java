package com.moviereviews.server.domain.mapper;

public interface OneToOneConverter<To, From> {
    To convert(From from);
}
