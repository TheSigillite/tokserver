package com.moviereviews.server.domain.mapper.movie;

import com.moviereviews.server.domain.dto.movie.UpdateMovieDto;
import com.moviereviews.server.domain.entity.Movie;
import com.moviereviews.server.domain.mapper.OneToOneConverter;
import org.springframework.stereotype.Component;

@Component
public class UpdateMovieDtoMapper implements OneToOneConverter<Movie, UpdateMovieDto> {
    @Override
    public Movie convert(UpdateMovieDto updateMovieDto) {
        return new Movie.Builder()
                .movie_id(updateMovieDto.getMovie_id())
                .title(updateMovieDto.getTitle())
                .cover(updateMovieDto.getCover())
                .director(updateMovieDto.getDirector())
                .premiere(updateMovieDto.getPremiere())
                .build();
    }
}
