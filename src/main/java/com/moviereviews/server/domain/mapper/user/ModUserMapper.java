package com.moviereviews.server.domain.mapper.user;

import com.moviereviews.server.domain.entity.User;
import com.moviereviews.server.domain.mapper.OneToOneConverter;
import org.springframework.stereotype.Component;

@Component
public class ModUserMapper implements OneToOneConverter<User, User> {
    @Override
    public User convert(User olduser) {
        return new User.Builder()
                .acc_id(olduser.getAcc_id())
                .login(olduser.getLogin())
                .passwd(olduser.getPasswd())
                .is_adm(true).build();
    }
}
