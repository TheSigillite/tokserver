package com.moviereviews.server.domain.mapper.review;

import com.moviereviews.server.domain.dto.review.ReviewDto;
import com.moviereviews.server.domain.entity.Review;
import com.moviereviews.server.domain.entity.User;
import com.moviereviews.server.domain.mapper.TwoToOneConverter;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ReviewListMapper implements TwoToOneConverter<List<ReviewDto>,List<Review>,List<User>> {
    @Override
    public List<ReviewDto> convert(List<Review> reviews, List<User> users) {
        return reviews.stream().map(review -> {
            String login = "";
            for (User user: users) {
                if (review.getAcc_id().equals(user.getAcc_id())) {
                    login = user.getLogin();
                }
            }
            return new ReviewDto.Builder()
                    .movie_id(review.getMovie_id())
                    .login(login)
                    .rev(review.getRev())
                    .Build();
        }).collect(Collectors.toList());
    }
}
