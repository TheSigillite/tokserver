package com.moviereviews.server.domain.mapper.review;

import com.moviereviews.server.domain.dto.review.NewReviewDto;
import com.moviereviews.server.domain.entity.Review;
import com.moviereviews.server.domain.entity.User;
import com.moviereviews.server.domain.mapper.TwoToOneConverter;
import org.springframework.stereotype.Component;

@Component
public class NewReviewMapper implements TwoToOneConverter<Review, User, NewReviewDto> {

    @Override
    public Review convert(User user, NewReviewDto newReviewDto) {
        return new Review.Builder()
                .acc_id(user.getAcc_id())
                .movie_id(newReviewDto.getMovie_id())
                .rev(newReviewDto.getRev())
                .build();
    }
}
