package com.moviereviews.server.domain.mapper;

public interface TwoToOneConverter<To,From1,From2> {
    To convert(From1 from1, From2 from2);
}
