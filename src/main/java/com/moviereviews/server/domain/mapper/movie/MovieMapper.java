package com.moviereviews.server.domain.mapper.movie;

import com.moviereviews.server.domain.dto.movie.MovieDto;
import com.moviereviews.server.domain.entity.Movie;
import com.moviereviews.server.domain.mapper.OneToOneConverter;
import org.springframework.stereotype.Component;

@Component
public class MovieMapper implements OneToOneConverter<MovieDto, Movie> {
    @Override
    public MovieDto convert(Movie movie) {
        return new MovieDto.Builder()
                .movie_id(movie.getMovie_id())
                .title(movie.getTitle())
                .cover(movie.getCover())
                .director(movie.getDirector())
                .premiere(movie.getPremiere())
                .build();
    }
}
