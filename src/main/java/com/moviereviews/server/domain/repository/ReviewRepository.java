package com.moviereviews.server.domain.repository;


import com.moviereviews.server.domain.entity.Review;
import com.moviereviews.server.domain.entity.ReviewId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface ReviewRepository extends JpaRepository<Review, ReviewId>, CrudRepository<Review,ReviewId> {
    @Modifying
    @Query(value = "DELETE FROM reviews r WHERE r.movie_id = :movie_id", nativeQuery = true)
    void deleteByMovie_id(@Param("movie_id") Long movie_id);

    @Query(value = "SELECT * FROM reviews r WHERE r.movie_id = :movie_id", nativeQuery = true)
    List<Review> findReviewsByMovie_id(@Param("movie_id") Long movie_id);

    @Modifying
    @Query(value = "DELETE FROM reviews r WHERE r.movie_id = :movie_id AND r.rev = :rev", nativeQuery = true)
    void deleteByMovie_idAndRev(@Param("movie_id") Long movie_id,@Param("rev") String rev);

    @Query(value = "SELECT * FROM reviews r WHERE r.movie_id = :movie_id AND r.rev = :rev", nativeQuery = true)
    Review findReviewByMovie_idAndRev(@Param("movie_id") Long movie_id,@Param("rev") String rev);

    @Modifying
    @Query(value = "INSERT INTO reviews(movie_id, acc_id, rev) VALUES (:movie_id, :acc_id, :rev)", nativeQuery = true)
    void insertReview(@Param("movie_id") Long movie_id, @Param("acc_id") Long acc_id, @Param("rev") String rev);
}
